const sum = (arr) => {
  return arr.reduce((a,b) => a + b , 0)
}
const rest = (a , b) => a - b 

module.exports = {
  sum: sum,
  res: rest
}
