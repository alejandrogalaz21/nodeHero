
const fs = require('fs')

let content 
try {
  content = fs.readFileSync('./readme.md', 'utf-8')
}
catch (e) {
  console.log('error reading the file')
  console.log(e)
}
console.log(content)

//read file ASYNC
console.log('START READING THE FILE')
fs.readFile('./readme.md' , 'utf-8', (err, content) => {
  console.log(`\n\t******************* FILE READER ASYNC *******************`)
  if (err){
   console.log('error happened during reading the file') 
   return console.log(err)
  }
  console.log(content)
  console.log(`\n\t****************** /FILE READER ASYNC *******************`)
})