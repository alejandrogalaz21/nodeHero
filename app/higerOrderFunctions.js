/*
 *  higher-order functions :
 *  Functions that can take other functions as arguments are called.
*/

const numbers = [2,4,1,5,4]

function isBiggerThanTwo (num) {
  return num > 2
}

const result = numbers.filter(isBiggerThanTwo)

console.log(`\n\t******************* higer order functions *******************`)
console.log(`\t ${result}`)
console.log(`\n\t****************** /higer order functions *******************`)
