// clean the terminal
console.log('\033c')
// index.js
require('./app/index')

const _ = require('lodash')
const fs = require('fs')

const merged = _.assign({ 'a': 1 }, { 'b': 2 }, { 'c': 3 })
console.log(`merged =`)
console.log(merged)

console.log(`\n\t******************* FILE READER *******************`)
//reader.js
require('./app/reader')
console.log(`\n\t****************** /FILE READER *******************`)

//another aproach to import scripts
require('./app/higerOrderFunctions')